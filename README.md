# Immo predict backend

Extract data from [empire immo](https://www.empireimmo.com/) with puppeteer

## Requirements

- node
- google chrome
- docker

## Installation

```bash
yarn install
```

## Local run

1. Modify .env.example file to fit with your datas
2. Start mongodb server with docker compose
3. Then start nodejs program

```bash
docker-compose -f localdev/docker-compose.yml up -d 
yarn start

#to stop
docker-compose -f localdev/docker-compose.yml down
```

## Deploy

Immo predict is automaticaly deployed on Kubernetes from Gitlab ci/cd when branch is merged on master