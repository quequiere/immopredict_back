module.exports = { doAjaxRequest };

const authService = require("../service/authentication/AuthenticationService")
const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));


async function doAjaxRequest(url, body) {
    return await fetch(url, {
        headers: {
            "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
            "x-requested-with": "XMLHttpRequest",
            credentials: 'include',
            cookie: authService.getStringCookies()
        },
        "body": body,
        method: "POST"
    }).catch(err => {
        console.log("Failed execute ajax request", err)
        throw err
    }).then(result => result.text())
}
