module.exports = { loadSellPercent };

const AjaxRequest = require("./AjaxRequest")


async function loadSellPercent() {
    let htmlResult = await AjaxRequest.doAjaxRequest(`${process.env.IMMO_BASE_URL}/properties/ajax_management.php`
        , "action=info&unityId=3111&unityType=1")

    var localpage = await global.browser.newPage();
    await localpage.setContent(htmlResult)

    let res = await localpage.$$(".header").then( e => e.map( el =>  el.evaluate(t => t.innerText)))

    const percentText = await Promise.all(res).then(e => e.find(t => t.includes("actuellement de")));

    const finalPercent = percentText.split('actuellement de ')[1].replace("%.", "")

    await localpage.close()
    return parseInt(finalPercent)
}