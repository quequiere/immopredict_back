
module.exports = { start };

const buildingService = require("./dataServices/BuildingService")
const metadataService = require("./dataServices/MetadataService")
const programStatusService = require("./dataServices/ProgramStatusService")
const persistantService = require("./datastorage/PersistantService")
const restApi = require("../web/restApi")

var CronJob = require('cron').CronJob;


async function start() {
    restApi.start()

    await refreshData()
    programStatusService.setReady(true)

    var job = new CronJob('5 0,14,15,29,30,44,45,59 * * * *', async function () {
        await refreshData()
    }, null, true, 'Europe/Paris');
    job.start();
}

async function refreshData() {
    console.log("=== Start refresh data ===", new Date().toISOString())
    await metadataService.reloadMetadata()
    await buildingService.reloadBuidlings()
    programStatusService.updateLastRefresh()
    console.log("=== Refresh finished ===", new Date().toISOString())

    await persistantService.saveCurrentData()
}

