module.exports = { doAuth, getStringCookies };
const fs = require('fs').promises;

var cachedCookies = null;

async function doAuth() {
    await reloadCookies()
    await tryAuth()
}

async function reloadCookies() {

    var cookies = await fs.readFile('./cookies.json').catch(e => {})
    if(cookies === undefined) return
    
    cookies = JSON.parse(cookies)
    await global.page.setCookie(...cookies)

}

async function tryAuth() {

    await global.page.goto(`${process.env.IMMO_BASE_URL}/login.php`);
    if(global.page.url().includes("login.php")){

        await global.page.$eval('#txtLogin', (el, username) => el.value = username, process.env.IMMO_USERNAME);
        await global.page.$eval('#txtPassword', (el, password) => el.value = password, process.env.IMMO_PASSWORD);
        await global.page.click('[name=form_login] button')
        await global.page.waitForSelector('#EIContent', { timeout: 4000 })
    
        if(global.page.url()!=`${process.env.IMMO_BASE_URL}/home.php`) throw new Error(`Failed to authenticate client, url is ${global.page.url()}`)

        await saveSession()
        console.log("[AUTHENTICATION] User is now logged")
    }
    else{
        console.log("[AUTHENTICATION] User already logged")
    }
    
    cachedCookies = await global.page.cookies()
}

async function saveSession() {
    const cookies = await global.page.cookies();
    await fs.writeFile('./cookies.json', JSON.stringify(cookies, null, 2));
    console.log("cookies saved")
}

function getStringCookies() {
    let result =  "";
    for(let c of cachedCookies){
        result+= `${c.name}=${c.value}; `
    }
    return result
}