module.exports = { reloadBuidlings, getBuidlings };

const fastDataExtractor = require("../../extractors/FastDataExtractor")

var buildings = []


function getBuidlings() {
    return buildings
}

async function reloadBuidlings() {

    buildings = await fastDataExtractor.createBatiment();

    await fastDataExtractor.enrichedWithBuildData(buildings)
    await fastDataExtractor.enrichedWithEmbellissementdData(buildings)

    buildings.filter(b => !b.isTerrain).forEach(b => {
        b.nextBuildingId = b.getNext()
        b.previousBuildingId = b.getPrevious()
    })


    console.log("=== Fully updated building data ===")

}

function getBuildByName(name) {
    return buildings.filter(b => b.name == name)[0]
}

function getBuildById(id) {
    return buildings.filter(b => b.id == id)[0]
}



