module.exports = { reloadMetadata, getMetadata };

const metadataExtractor = require("../../extractors/MetadataExtractor")

var percent = -1

function getMetadata(){
    return {
        promoteurPercent: percent
    }
}

async function reloadMetadata(){
    await reloadSellPercent()
}

async function reloadSellPercent(){
    let result = await metadataExtractor.loadSellPercent()
    percent = result / 100
}

