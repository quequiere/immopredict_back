module.exports = { setReady, updateLastRefresh, getStatus };

var isReady = false
var lastDataUpdate = 0

function getStatus(){
    return {
        isReady : isReady,
        lastDataUpdate: lastDataUpdate
    }
}

function setReady(status){
    isReady = status
}

function updateLastRefresh(){
    lastDataUpdate = Date.now()
}