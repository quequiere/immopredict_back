module.exports = { priceToInt };

function priceToInt(price) {
  return parseInt(price.replace(/Ø/g,"").replace(/ /g,""))
}
