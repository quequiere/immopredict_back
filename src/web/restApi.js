module.exports = { start };
const buildingService = require("../service/dataServices/BuildingService")
const metadataService = require("../service/dataServices/MetadataService")
const programStatusService = require("../service/dataServices/ProgramStatusService")
const persistantService = require("../service/datastorage/PersistantService")

const cors = require('cors')
const express = require('express')
const app = express()
const port = 8080


async function start() {
    await initEndpoints()
}

async function initEndpoints() {

    app.use(cors())

    app.get('/api/metadata', (req, res) => {
        res.send(metadataService.getMetadata())
    })

    app.get('/api/buidlings/list', (req, res) => {
        res.send(buildingService.getBuidlings())
    })


    app.get('/api/status', (req, res) => {
        res.send(programStatusService.getStatus())
    })

    app.get('/api/history/promoteur', async (req, res) => {
        res.send(await persistantService.getPromoteurHistoryLight())
    })



    app.get('/api/buidlings/:batid/dispo', async (req, res) => {
        let realId = parseInt(req.params.batid)
        if (!realId) {
            return res.status(400).send({
                message: 'Batimend id not found'
            });
        }
        let result = await persistantService.getStockInfoByBatimentId(realId)
        res.send(result)
    })

    app.get('/api/buidlings/:batid/bought', async (req, res) => {
        let realId = parseInt(req.params.batid)
        if (!realId) {
            return res.status(400).send({
                message: 'Batimend id not found'
            });
        }
        let result = await persistantService.getBoughtQuantityForBatidByDay(realId)
        res.send(result)
    })

    app.get('/api/buidlings/:batid/price', async (req, res) => {
        let realId = parseInt(req.params.batid)
        if (!realId) {
            return res.status(400).send({
                message: 'Batimend id not found'
            });
        }
        let result = await persistantService.getBatimentPriceById(realId)
        res.send(result)
    })


    app.listen(port, () => {
        console.log(`Start API on port ${port}`)
    })
}